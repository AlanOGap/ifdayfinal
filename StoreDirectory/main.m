//
//  main.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDAppDelegate class]));
    }
}
