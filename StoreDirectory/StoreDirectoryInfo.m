//
//  StoreDirectoryInfo.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/15/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "StoreDirectoryInfo.h"


@implementation StoreDirectoryInfo

@dynamic address;
@dynamic storeNumber;
@dynamic city;
@dynamic state;
@dynamic brand;

@end
