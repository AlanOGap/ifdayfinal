//
//  StoreDirectoryDataAccess.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/15/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "StoreDirectoryDataAccess.h"
#import "StoreDirectoryInfo.h"
#import "SDAppDelegate.h"

@implementation StoreDirectoryDataAccess
@synthesize managedObjectContext = _managedObjectContext;


-(StoreDirectoryDataAccess*) initWithContext: (NSManagedObjectContext *)managedObjectContext {
    
    self = [super init];
    if(self) {
        _managedObjectContext = managedObjectContext;
    }
    return self;
    
}



-(NSArray *)findStoreInfoByBrandAndCity:(NSString*) brand :(NSString*)city {
    NSLog(@"##########BYBRANDANDCITY###############");
        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StoreDirectoryInfo" inManagedObjectContext:self.managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"(brand LIKE[cd] %@) AND (city LIKE[cd] %@)",brand, city];
        [request setEntity:entityDesc];
        [request setPredicate:pred2];
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (StoreDirectoryInfo *info in fetchedObjects) {
        NSLog(@"Store Number: %@", info.storeNumber);
       // NSManagedObject *details = [info valueForKey:@"details"];
        NSLog(@"Address: %@", info.address);
    }
    
    
            return fetchedObjects;
   
//    return NULL;
    
}


-(NSArray *)findStoreInfoByStoreNumber:(NSString*)storeNumber {
NSLog(@"##########BYSTORENUMBER###############");
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StoreDirectoryInfo" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"storeNumber LIKE[cd] %@",storeNumber];
    [request setEntity:entityDesc];
    [request setPredicate:pred2];
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
        for (StoreDirectoryInfo *info in fetchedObjects) {
            NSLog(@"Store Number: %@", info.storeNumber);
           // NSManagedObject *details = [info valueForKey:@"details"];
            NSLog(@"Address: %@", info.address);
        }
    
    
    return fetchedObjects;
    
    //    return NULL;
    
}

-(NSArray *)findStoreInfoByBrand:(NSString*)brand {
    NSLog(@"##########BYBRAND###############");
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StoreDirectoryInfo" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"brand LIKE[cd] %@",brand];
    [request setEntity:entityDesc];
    [request setPredicate:pred2];
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
        for (StoreDirectoryInfo *info in fetchedObjects) {
            NSLog(@"Store Number: %@", info.storeNumber);
           // NSManagedObject *details = [info valueForKey:@"details"];
            NSLog(@"Address: %@", info.address);
        }
    
    
    return fetchedObjects;
    
    //    return NULL;
    
}


-(NSArray *)findStoreInfoByCity:(NSString*)city {
    NSLog(@"##########BYCITY###############");
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"StoreDirectoryInfo" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"city LIKE[cd] %@",city];
    [request setEntity:entityDesc];
    [request setPredicate:pred2];
    NSError *error;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
        for (StoreDirectoryInfo *info in fetchedObjects) {
            NSLog(@"Store Number: %@", info.storeNumber);
           // NSManagedObject *details = [info valueForKey:@"details"];
            NSLog(@"Address: %@", info.address);
        }
    
    
    return fetchedObjects;
    
    //    return NULL;
    
}



@end
