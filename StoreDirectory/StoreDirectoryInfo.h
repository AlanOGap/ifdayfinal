//
//  StoreDirectoryInfo.h
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/15/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StoreDirectoryInfo : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * storeNumber;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * brand;

@end
