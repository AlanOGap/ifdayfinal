//
//  SDOpeningViewController.h
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDOpeningViewController : UIViewController
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end
