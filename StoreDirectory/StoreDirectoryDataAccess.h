//
//  StoreDirectoryDataAccess.h
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/15/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreDirectoryInfo.h"

@interface StoreDirectoryDataAccess : NSObject
@property StoreDirectoryInfo *coll;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

-(NSArray *)findStoreInfoByBrandAndCity:(NSString*) brand :(NSString*) city;
-(NSArray *)findStoreInfoByCity:(NSString*) city;
-(NSArray *)findStoreInfoByBrand:(NSString*) brand;
-(NSArray *)findStoreInfoByStoreNumber:(NSString*) storeNumber;
-(StoreDirectoryDataAccess*) initWithContext: (NSManagedObjectContext *)managedObjectContext;
@end
