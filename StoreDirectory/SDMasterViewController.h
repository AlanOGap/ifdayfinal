//
//  SDMasterViewController.h
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDAppDelegate.h"
#import "StoreInfoTableViewCell.h"

@interface SDMasterViewController : UITableViewController
@property (strong,nonatomic) SDAppDelegate *appDelegate;      //app Delegate ref for CoreData
@property (strong,nonatomic) NSManagedObjectContext *context;         //Managed Object context
@property (nonatomic, strong) NSMutableArray* storeDirectoryResults;

-(void) storeDirectoryResults: (NSArray*) withArray;
@end
