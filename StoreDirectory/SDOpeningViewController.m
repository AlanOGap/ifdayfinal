//
//  SDOpeningViewController.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "SDOpeningViewController.h"
#import "StoreDirectoryDataAccess.h"
#import "SDMasterViewController.h"

@interface SDOpeningViewController ()
@property (weak, nonatomic) IBOutlet UITextField *storeNumber;

@property (weak, nonatomic) IBOutlet UITextField *storeBrand;
@property (weak, nonatomic) IBOutlet UITextField *storeCity;
@property (strong, nonatomic) NSArray *results;
@property (strong,nonatomic) StoreDirectoryDataAccess *myData;


@end

@implementation SDOpeningViewController

@synthesize managedObjectContext;
@synthesize results;
@synthesize myData;
- (IBAction)searchPressed:(id)sender {
    NSLog(@"Store number : %@",_storeNumber);
   //results = [myData findStoreInfoByStoreNumber:_storeNumber.text];
   // [myData findStoreInfoByBrandAndCity:<#(NSString *)#> :<#(NSString *)#>
    
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    results = [myData findStoreInfoByStoreNumber:_storeNumber.text];
    [segue.destinationViewController storeDirectoryResults :results  ];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    myData = [[StoreDirectoryDataAccess alloc] initWithContext:managedObjectContext];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
